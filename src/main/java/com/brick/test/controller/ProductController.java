package com.brick.test.controller;

import com.brick.test.constant.ApiPath;
import com.brick.test.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;

@Slf4j
@RestController
@RequestMapping(ApiPath.PRODUCT_ROUTE)
@Api(value = "Product")
public class ProductController {

  @Autowired
  private ProductService productService;

  @ApiOperation(value = "Get List Product")
  @GetMapping(produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
  public ResponseEntity<ByteArrayResource> findProductsFile() {
    String filename = "Product_" + Calendar.getInstance().getTime().getTime() + ".csv";
    return ResponseEntity.ok()
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"")
        .body(productService.getProductsCsv());
  }
}
