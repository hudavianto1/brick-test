package com.brick.test.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product {

  private String productName;
  private String description;
  private String imageUrl;
  private BigDecimal price;
  private Double rate;
  private String storeName;
}
