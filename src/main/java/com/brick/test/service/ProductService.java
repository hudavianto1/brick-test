package com.brick.test.service;

import org.springframework.core.io.ByteArrayResource;

public interface ProductService {

  ByteArrayResource getProductsCsv();
}
