package com.brick.test.service.impl;

import com.brick.test.configuration.TokopediaProperties;
import com.brick.test.dto.Product;
import com.brick.test.helper.CsvUtil;
import com.brick.test.helper.TokopediaScrapperUtil;
import com.brick.test.service.ProductService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ProductServiceImpl implements ProductService {

  @Autowired
  private TokopediaProperties tokopediaProperties;

  @Override
  public ByteArrayResource getProductsCsv() {
    return CsvUtil.generateCsv(tokopediaProperties.getCsvHeader(),
        TokopediaScrapperUtil.findProducts(tokopediaProperties), Product.class);
  }
}
